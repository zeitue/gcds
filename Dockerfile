FROM eclipse-temurin:latest
LABEL maintainer="zeitue@gmail.com"
COPY ./options.varfile /tmp/gcds/options.varfile
RUN apt update && apt install cron && apt clean
RUN cd /tmp/gcds && \
    wget "https://dl.google.com/dirsync/dirsync-linux64.sh" && \
    chmod +x dirsync-linux64.sh && \
    (echo "o"; echo ""; echo "1"; echo ""; echo "y"; echo ""; echo "";) | \
    ./dirsync-linux64.sh -varfile ./options.varfile && \
    rm -Rf /tmp/gcds

COPY gcds /usr/local/bin
COPY ./gcds-cron /etc/cron.d/gcds-cron
RUN chmod 0644 /etc/cron.d/gcds-cron && \
    crontab /etc/cron.d/gcds-cron && \
    touch /var/log/cron.log
ARG TAG
ENV TAG=${TAG}
ENV CONFIG_FILE=
ENV OAUTH_KEY=
ENV AUTOUPDATE_OAUTH_KEY=

VOLUME [ "/config" ]
VOLUME [ "/logs" ]
CMD [ "cron", "-f" ]

